import imutils
import cv2
import numpy as np
from numba import jit

FRAMES_TO_PERSIST = 10

MIN_SIZE_FOR_MOVEMENT = 2000

MOVEMENT_DETECTED_PERSISTENCE = 100


@jit(nopython=True)
def func(transient_movement_flag, movement_persistent_counter):
    if transient_movement_flag:
        movement_persistent_counter = MOVEMENT_DETECTED_PERSISTENCE

    if movement_persistent_counter > -1:
        text = "Movements : " + str(movement_persistent_counter)
        print(text)
        movement_persistent_counter -= 10
    else:
        text = "No movements"

    return text, movement_persistent_counter


def main():
    global FRAMES_TO_PERSIST, MIN_SIZE_FOR_MOVEMENT, MOVEMENT_DETECTED_PERSISTENCE

    # cap = cv2.VideoCapture('rtsp://admin:@192.168.1.10/1')
    cap = cv2.VideoCapture('tmp/finch1.mp4')
    _, frame = cap.read()
    frame = imutils.resize(frame, height=640, width=480)
    bbox = cv2.selectROI("Tracking", frame, False)

    back_sub = cv2.createBackgroundSubtractorMOG2(history=700,
                                                  varThreshold=25, detectShadows=True)
    kernel = np.ones((20, 20), np.uint8)

    first_frame = None
    next_frame = None

    font = cv2.FONT_HERSHEY_SIMPLEX
    delay_counter = 0
    movement_persistent_counter = 0

    while True:

        transient_movement_flag = False

        _, frame = cap.read()

        frame = imutils.resize(frame, height=640, width=480)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        gray = cv2.GaussianBlur(gray, (21, 21), 0)

        fg_mask = back_sub.apply(frame)

        # Close dark gaps in foreground object using closing
        fg_mask = cv2.morphologyEx(fg_mask, cv2.MORPH_CLOSE, kernel)

        # Remove salt and pepper noise with a median filter
        fg_mask = cv2.medianBlur(fg_mask, 5)

        # Threshold the image to make it either black or white
        _, fg_mask = cv2.threshold(fg_mask, 127, 255, cv2.THRESH_BINARY)

        # Find the index of the largest contour and draw bounding box
        fg_mask_bb = fg_mask
        contours, hierarchy = cv2.findContours(fg_mask_bb, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2:]
        areas = [cv2.contourArea(c) for c in contours]

        x, y, w, h = [int(i) for i in bbox]
        cv2.rectangle(frame, (x, y), ((x + w), (y + h)), (255, 0, 255), 3, 1)

        if first_frame is None:
            first_frame = gray

        delay_counter += 1

        if delay_counter > FRAMES_TO_PERSIST:
            delay_counter = 0
            first_frame = next_frame

        next_frame = gray

        frame_delta = cv2.absdiff(first_frame, next_frame)
        thresh = cv2.threshold(frame_delta, 25, 255, cv2.THRESH_BINARY)[1]

        thresh = cv2.dilate(thresh, None, iterations=2)
        cnts, _ = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        for c in cnts:

            (x, y, w, h) = cv2.boundingRect(c)

            if cv2.contourArea(c) > MIN_SIZE_FOR_MOVEMENT:
                if (x + w > bbox[0] + 3 and y + h > bbox[1] + 3) and (
                        x + w < bbox[0] + bbox[2] - 3 and y + h < bbox[1] + bbox[3] - 3):
                    transient_movement_flag = True
                    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            else:
                try:
                    max_index = np.argmax(areas)
                    cnt = contours[max_index]
                    x, y, w, h = cv2.boundingRect(cnt)
                    if (x + w > bbox[0] + 3 and y + h > bbox[1] + 3) and (
                            x + w < bbox[0] + bbox[2] - 3 and y + h < bbox[1] + bbox[3] - 3):
                        transient_movement_flag = True
                        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                except:
                    pass

        text, movement_persistent_counter = func(transient_movement_flag, movement_persistent_counter)

        cv2.putText(frame, text, (10, 35), font, 0.75, (255, 255, 255), 2, cv2.LINE_AA)

        frame_delta = cv2.cvtColor(frame_delta, cv2.COLOR_GRAY2BGR)

        cv2.imshow("Tracking", np.hstack((frame_delta, frame)))

        ch = cv2.waitKey(1)
        if ch & 0xFF == 113:
            break

    cv2.waitKey(0)
    cv2.destroyAllWindows()
    cap.release()


if __name__ == '__main__':
    main()
